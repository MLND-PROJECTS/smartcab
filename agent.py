import random
from environment import Agent, Environment,DummyAgent
from planner import RoutePlanner
from simulator import Simulator
import numpy as np

class LearningAgent(Agent):
    """An agent that learns to drive in the smartcab world."""
    light_list = ['red','green']
    valid_actions = [None, 'forward', 'left', 'right']

    def __init__(self, env):
        super(LearningAgent, self).__init__(env)  # sets self.env = env, state = None, next_waypoint = None, and a default color
        self.color = 'red'  # override color
        self.planner = RoutePlanner(self.env, self)  # simple route planner to get next_waypoint
        # TODO: Initialize any additional variables here
        self.alpha = 0.3
        self.gamma = 0.3
        self.epsilon = 0.2
        
        self.Q_dict = dict()
        for next_waypoint in self.valid_actions:
            for light in self.light_list:
                for oncoming in self.valid_actions:
                    for left in self.valid_actions:
                        for right in self.valid_actions:
                            state = (next_waypoint,light,oncoming,left,right)
                            for action in self.valid_actions:
                                self.Q_dict[(state,action)] = 0
        
    def reset(self, destination=None):
        self.planner.route_to(destination)
        # TODO: Prepare for a new trip; reset any variables here, if required
        
    def update(self, t):
        # Gather inputs
        self.next_waypoint = self.planner.next_waypoint()  # from route planner, also displayed by simulator
        inputs = self.env.sense(self)
        deadline = self.env.get_deadline(self)

        # TODO: Update state
        self.state=(self.next_waypoint,inputs['light'],inputs['oncoming'],inputs['left'],inputs['right'])
        
        # TODO: Select action according to your policy
        action = self.get_action(self.state,self.Q_dict)[0]

        # Execute action and get reward
        reward = self.env.act(self, action)


        # TODO: Learn policy based on state, action, reward
        inputs = self.env.sense(self)
        next_state = (self.planner.next_waypoint(),inputs['light'],inputs['oncoming'],inputs['left'],inputs['right'])
        self.Q_dict[(self.state,action)]=(1-self.alpha)* self.Q_dict[(self.state,action)]+self.alpha*(reward+self.alpha*self.gamma*self.get_action(next_state,self.Q_dict)[1])

        #print "LearningAgent.update(): deadline = {}, inputs = {}, action = {}, reward = {}".format(deadline, inputs, action, reward)  # [debug]


    def get_action(self,state,Q_dict):
        action_dict=dict()
        for ac in self.valid_actions:
            action_dict[(state,ac)] = Q_dict[(state,ac)]
        Qmax = max(action_dict.values())
        if random.random > self.epsilon:
           
            action = random.choice([k for k,v in action_dict.items() if v == Qmax])[1]
        else:
            action = random.choice(self.valid_actions)
        return action,Qmax



def run():
    """Run the agent for a finite number of trials."""

    # Set up environment and agent
    e = Environment()  # create environment (also adds some dummy traffic)
    a = e.create_agent(LearningAgent)  # create agent
    e.set_primary_agent(a, enforce_deadline=True)  # specify agent to track
    # NOTE: You can set enforce_deadline=False while debugging to allow longer trials

    # Now simulate it
    sim = Simulator(e, update_delay=0.5, display=True)  # create simulator (uses pygame when display=True, if available)
    # NOTE: To speed up simulation, reduce update_delay and/or set display=False

    sim.run(n_trials=100)  # run for a specified number of trials
    # NOTE: To quit midway, press Esc or close pygame window, or hit Ctrl+C on the command-line


if __name__ == '__main__':
    run()
